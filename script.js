const out = document.querySelector('.calc__screen p')

let a = ''
//?=============================================================
function onButtonClick(key) {
	a = a + key
	out.textContent = a
	console.log(a)
}
//?=============================================================
function onClearClick() {
	clearAll()
}
function clearAll() {
	a = ''
	out.textContent = '0'
}
//?=============================================================
function onEqualClick() {
	if (eval(a)) {
		a = `${eval(a)}`
		console.log(a)
		out.textContent = a
	}

}
//?=============================================================
function onBackClick() {
	a = a.substring(0, a.length - 1)
	out.textContent = a
}